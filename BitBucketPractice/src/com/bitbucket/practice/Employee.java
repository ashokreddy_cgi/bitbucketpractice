package com.bitbucket.practice;

public class Employee {
	//This is Employee pojo class
	String name;
	int age;
	int ph;
//	String address;
	
	Employee(String name,int age)
	{
		this.name=name;
		this.age=age;
	
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}

}
